import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		data: {
			event: {},
			selection: {
				current: [],
				next: [],
				previous: [],
			},
			eightBucks: {
				battle: {},
				dancers: [],
			},
			battle: {
				redDancer: {},
				blueDancer: {},
				votes: {
					redVotes: 0,
					blueVotes: 0,
				},
			},
			dancers: [],
			type: '',
			srvUri: 'http://104.248.244.225:8000',
		},
	},
	getters: {
		currentView(state) {
			switch(state.data.type) {
				case 'selection': return 'Hatplayer';
				case 'battle': return 'Judgement';
				case 'eightBucks': return 'Bucks';
				case 'votes': return 'Votes';
				default: return 'BeReady';
			}
		},
	},
	mutations: {
		setData (state, data) {
			state.data = Object.assign(state.data, data);
		},
	},
	actions: {
		socketConnect(context) {
			const ws = new WebSocket('ws://104.248.244.225:8000/desktop/');
			ws.onopen = () => console.log('ws connected');
			ws.onerror = (error) => console.dir(error);
			ws.onmessage = (event) => {
				context.commit('setData', JSON.parse(event.data));
				console.log(context.state.data, JSON.parse(event.data));
			}
		}
	}
});
